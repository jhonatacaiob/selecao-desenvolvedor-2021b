## Prova de SQL
A prova de SQL por simplicidade será feita em SQLite, seu banco de dados se encontra neste repositório, sendo ele
o arquivo `backend/chinook.db`, e caso você não possua um cliente SQLite, você pode encontrar um no seguinte link:
https://portableapps.com/apps/development/sqlite_database_browser_portable

Você pode encontrar um diagrama sobre o que tem disponível neste banco na imagem `chinook_db_schema.jpg`.

## Questões
1 - Utilizando o banco de dados fornecido no arquivo chinook.db prepare uma consulta que mostre o nome do artista e a quantidade albuns que ele lançou, ordenados por quem mais tem albuns.
    Colunas da resposta:
        ArtistName | QtdeAlbums

    Resposta:   SELECT ar.Name as ArtistName,
                    COUNT(al.AlbumId) as QtdeAlbums
                FROM artists AS ar
                JOIN albums as al
                ON ar.ArtistId = al.ArtistId
                
                GROUP BY ar.ArtistId ORDER BY QtdeAlbums DESC

2 - Prepare uma consulta que traga os 20 clientes que mais gastaram. (Na tabela invoices há as compras feitas por cada cliente e seu valor)
    Colunas da resposta:
        CustomerFullName | Total

    Resposta:   SELECT c.FirstName || ' ' || c.LastName AS CustomerFullName,
                    SUM(ivc.Total) as Total
                FROM invoices AS ivc
                JOIN customers AS c
                ON ivc.CustomerId = c.CustomerId
                
                GROUP BY ivc.CustomerId ORDER BY Total DESC
                LIMIT 20

3 - Listar gênero musical com o valor vendido, e a quantidade de vendas.
    Colunas da resposta:
        Genre | TotalSold | QtdeSold

    Resposta:   SELECT g.Name AS Genre, SUM(ivc_it.UnitPrice) AS TotalSold,
                    COUNT(ivc_it.InvoiceId) AS QtdeSold
                FROM tracks AS t
                JOIN genres AS g 
                ON t.GenreId = g.GenreId
                
                JOIN invoice_items AS ivc_it
                ON ivc_it.TrackId = t.TrackId
                
                GROUP BY t.GenreId

4 - Listar os albuns com preço, duração total em minutos, tamanho em MB.
    Colunas da resposta:
        AlbumTitle | Price | Duration_minutes | Size_MB
    
    Resposta:   SELECT al.Title AS AlbumTitle,
                    SUM(tr.UnitPrice) AS Price,
                    (SUM(tr.Milliseconds)/60000) AS Duration_minutes,
                    (SUM(tr.Bytes) / (1024*1024)) as Size_MB
                FROM tracks AS tr 
                
                JOIN albums AS al
                ON tr.AlbumId = al.ArtistId
                
                GROUP BY tr.AlbumId


5 - Listar empregados com números de clientes, quanto cada um vendeu até o momento, gênero musical que mais vende em qtd (mais popular), e em valor (mais lucrativo),  0
    Colunas da resposta:  
        EmployeeId | EmployeeFullName | QtdeCustomers | TotalSold | MostPopularGenre | MostLucrativeGenre   

    Resultado:  SELECT e.EmployeeId,
                    e.FirstName || ' ' || e.LastName AS EmployeeFullName,
                    COUNT(DISTINCT c.CustomersId) AS QtdeCustomers,
                    SUM(ivc.Total) AS TotalSold
                FROM employees AS e
                LEFT JOIN customers AS c
                ON e.EmployeeId = c.SupportRepId
                
                LEFT JOIN invoices AS ivc
                ON c.CustomerId = ivc.CustomerId
                
                LEFT JOIN invoice_items AS ivc_it
                ON ivc.InvoiceId = ivc_it.InvoiceId
                
                LEFT JOIN tracks AS t
                ON ivc_it.TrackId = t.TrackId
                
                LEFT JOIN genres AS g
                ON t.GenreId = g.GenreId
                
                GROUP BY e.EmployeeId
                *QUESTÃO PARCIALMENTE CONCLUIDA*

6 - Consulta que traga a lista de gêneros musicais com 12 colunas (Janeiro a Dezembro) com a qtd de faixas vendidas de um determinado ano a ser especificado num filtro.
    Colunas da resposta:
        GenreId | GenreName | Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec
   
    Resultado:  SELECT g.GenreId, g.Name AS GenreName,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '01' THEN '' END) AS Jan,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '02' THEN '' END) AS Feb,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '03' THEN '' END) AS Mar,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '04' THEN '' END) AS Apr,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '05' THEN '' END) AS May,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '06' THEN '' END) AS Jun,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '07' THEN '' END) AS Jul,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '08' THEN '' END) AS Aug,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '09' THEN '' END) AS Sep,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '10' THEN '' END) AS Oct,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '11' THEN '' END) AS Nov,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '12' THEN '' END) AS Dec
                FROM invoices AS ivc
                JOIN invoice_items AS ivc_it 
                ON ivc.InvoiceId = ivc_it.InvoiceId

                JOIN tracks AS t
                ON t.TrackId = ivc_it.TrackId
                
                JOIN genres AS g
                ON t.GenreId = g.GenreId
                
                WHERE strftime('%Y', ivc.InvoiceDate) = '2009'
                GROUP BY g.GenreId

7 - Listar supervisores (aqueles funcionários a quem os outros se reportam)
    Há um funcionário que não se reporta a ninguém, este não precisará vir na listagem.
    Há diversos funcionários que ninguém se reporta a eles, este também não devem vir na listagem.
    Aos funcionários que virão na listagem, deverá ser exibida o nome deles, e 12 colunas de meses (Jan-Dez) com o valor que foi vendido por este, ou por alguma das pessoas que se reportam a ele. E outras 12 colunas de meses com a quantidade de faixas vendidas por ele, ou alguém que se reporte a ele.
    Esta tabela será utilizada para gerar gráficos de rendimento das equipes de cada supervisor.
    Deverá conter também uma coluna listando quantos clientes aquela equipe possui.
    Colunas da resposta:
        EmployeeId | EmployeeFullName | QtdeCustomers | 
        TotalSold_Jan | TotalSold_Feb | TotalSold_Mar | TotalSold_Apr | TotalSold_May | TotalSold_Jun |
        TotalSold_Jul | TotalSold_Aug | TotalSold_Sep | TotalSold_Oct | TotalSold_Nov | TotalSold_Dec |
        QtdeTracksSold_Jan | QtdeTracksSold_Feb | QtdeTracksSold_Mar | QtdeTracksSold_Apr | QtdeTracksSold_May | QtdeTracksSold_Jun |
        QtdeTracksSold_Jul | QtdeTracksSold_Aug | QtdeTracksSold_Sep | QtdeTracksSold_Oct | QtdeTracksSold_Nov | QtdeTracksSold_Dec

    Resultado:  SELECT 
                    CASE 
                        WHEN e.ReportsTo IN (SELECT employees.ReportsTo 
                                                FROM employees 
                                                WHERE employees.ReportsTo IS NOT NULL 
                                                AND employees.ReportsTo != 1)
                        THEN
                            e.ReportsTo
                        ELSE 
                            e.EmployeeId
                        END Employee,
                    CASE 
                        WHEN e.ReportsTo IN (SELECT employees.ReportsTo 
                                            FROM employees 
                                            WHERE employees.ReportsTo IS NOT NULL 
                                            AND employees.ReportsTo != 1)
                        THEN
                            m.FirstName || ' ' || m.LastName
                        ELSE 
                            e.FirstName || ' ' || e.LastName
                        END EmployeeFullName,

                    COUNT(DISTINCT c.CustomerId) AS QtdeCustomers,

                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '01' THEN ivc.Total END) AS TotalSold_Jan,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '02' THEN ivc.Total END) AS TotalSold_Feb,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '03' THEN ivc.Total END) AS TotalSold_Mar,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '04' THEN ivc.Total END) AS TotalSold_Apr,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '05' THEN ivc.Total END) AS TotalSold_May,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '06' THEN ivc.Total END) AS TotalSold_Jun,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '07' THEN ivc.Total END) AS TotalSold_Jul,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '08' THEN ivc.Total END) AS TotalSold_Aug,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '09' THEN ivc.Total END) AS TotalSold_Sep,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '10' THEN ivc.Total END) AS TotalSold_Oct,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '11' THEN ivc.Total END) AS TotalSold_Nov,
                    SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '12' THEN ivc.Total END) AS TotalSold_Dec,

                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '01' THEN ivc.Total END) AS QtdeTracksSold_Jan,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '02' THEN ivc.Total END) AS QtdeTracksSold_Feb,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '03' THEN ivc.Total END) AS QtdeTracksSold_Mar,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '04' THEN ivc.Total END) AS QtdeTracksSold_Apr,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '05' THEN ivc.Total END) AS QtdeTracksSold_May,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '06' THEN ivc.Total END) AS QtdeTracksSold_Jun,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '07' THEN ivc.Total END) AS QtdeTracksSold_Jul,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '08' THEN ivc.Total END) AS QtdeTracksSold_Aug,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '09' THEN ivc.Total END) AS QtdeTracksSold_Sep,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '10' THEN ivc.Total END) AS QtdeTracksSold_Oct,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '11' THEN ivc.Total END) AS QtdeTracksSold_Nov,
                    COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '12' THEN ivc.Total END) AS QtdeTracksSold_Dec
                                        
                FROM employees AS e
                JOIN customers AS c
                ON e.EmployeeId = c.SupportRepId
                
                JOIN invoices AS ivc
                ON ivc.CustomerId = c.CustomerId
                
                JOIN invoice_items AS ivc_it
                ON ivc_it.InvoiceId = ivc.InvoiceId
                
                JOIN employees as m
                ON m.EmployeeId = e.ReportsTo
                
                GROUP BY Employee;
8 - Criar uma View que possibilite mostrar os dados da lista de supervisores mencionada acima (questão 7), e que possibilite ser filtrada por ano.
    Quero fazer a consulta simplesmente com `select * from vw_lista_supervisores where ano = 2015`.
    Atenção: A resolução dessa questão é a apresentação do script de criação dessa View. E não a criação dela dentro do banco de
        dados que se encontra neste repositório. Se o candidato apenas criar a view dentro do banco de dados mas não apresentar
        o script por escrito na prova, será considerado como não tendo respondido.

    Resultado: 
        CREATE TEMP VIEW IF NOT EXISTS vw_lista_supervisores
        AS
        SELECT  CASE 
                    WHEN e.ReportsTo IN (SELECT employees.ReportsTo 
                                            FROM employees 
                                            WHERE employees.ReportsTo IS NOT NULL 
                                            AND employees.ReportsTo != 1)
                    THEN
                        e.ReportsTo
                    ELSE 
                        e.EmployeeId
                    END Employee,
                CASE 
                    WHEN e.ReportsTo IN (SELECT employees.ReportsTo 
                                        FROM employees 
                                        WHERE employees.ReportsTo IS NOT NULL 
                                        AND employees.ReportsTo != 1)
                    THEN
                        m.FirstName || ' ' || m.LastName
                    ELSE 
                        e.FirstName || ' ' || e.LastName
                    END EmployeeFullName,
                    
                COUNT(DISTINCT c.CustomerId) AS QtdeCustomers,

                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '01' THEN ivc.Total END) AS TotalSold_Jan,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '02' THEN ivc.Total END) AS TotalSold_Feb,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '03' THEN ivc.Total END) AS TotalSold_Mar,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '04' THEN ivc.Total END) AS TotalSold_Apr,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '05' THEN ivc.Total END) AS TotalSold_May,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '06' THEN ivc.Total END) AS TotalSold_Jun,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '07' THEN ivc.Total END) AS TotalSold_Jul,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '08' THEN ivc.Total END) AS TotalSold_Aug,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '09' THEN ivc.Total END) AS TotalSold_Sep,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '10' THEN ivc.Total END) AS TotalSold_Oct,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '11' THEN ivc.Total END) AS TotalSold_Nov,
                SUM(CASE strftime('%m', ivc.InvoiceDate) WHEN '12' THEN ivc.Total END) AS TotalSold_Dec,

                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '01' THEN ivc.Total END) AS QtdeTracksSold_Jan,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '02' THEN ivc.Total END) AS QtdeTracksSold_Feb,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '03' THEN ivc.Total END) AS QtdeTracksSold_Mar,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '04' THEN ivc.Total END) AS QtdeTracksSold_Apr,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '05' THEN ivc.Total END) AS QtdeTracksSold_May,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '06' THEN ivc.Total END) AS QtdeTracksSold_Jun,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '07' THEN ivc.Total END) AS QtdeTracksSold_Jul,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '08' THEN ivc.Total END) AS QtdeTracksSold_Aug,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '09' THEN ivc.Total END) AS QtdeTracksSold_Sep,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '10' THEN ivc.Total END) AS QtdeTracksSold_Oct,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '11' THEN ivc.Total END) AS QtdeTracksSold_Nov,
                COUNT(CASE strftime('%m', ivc.InvoiceDate) WHEN '12' THEN ivc.Total END) AS QtdeTracksSold_Dec,
                CASE WHEN strftime('%Y', ivc.InvoiceDate) THEN CAST(strftime('%Y', ivc.InvoiceDate)AS INT) END AS ano
                                    
            FROM employees AS e
            JOIN customers AS c
            ON e.EmployeeId = c.SupportRepId
            
            JOIN invoices AS ivc
            ON ivc.CustomerId = c.CustomerId
            
            JOIN invoice_items AS ivc_it
            ON ivc_it.InvoiceId = ivc.InvoiceId
            
            JOIN employees as m
            ON m.EmployeeId = e.ReportsTo
            
            GROUP BY Employee, ano;