function NotaField({ maxNota, value, onChange }) {
    function onChangeValue (event) {
        const nota = event.target.value

        if (nota <= maxNota) {
            onChange(nota)
        }
    }
    let stars = [] 
    for (let i = 0; i < maxNota; i++){
      stars.push(
        <span 
          onClick = {() => onChange(i+1)}
          style={{
            filter:`grayscale(${value > i ? 0 : 1 })`,
            cursor:'pointer',
          }}
          >
          🌟
          </span>

          
      )  
    }
    return (
      <div>
        {stars}
        <input
            type={'number'}
            value={value}
            onChange={onChangeValue}
        />
      </div>
    );
  }
  
export default NotaField;
