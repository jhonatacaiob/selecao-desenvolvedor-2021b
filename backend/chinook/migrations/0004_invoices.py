# Generated by Django 3.2 on 2021-09-12 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chinook', '0003_delete_invoices'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoices',
            fields=[
                ('invoice_id', models.AutoField(db_column='InvoiceId', primary_key=True, serialize=False)),
                ('customer_id', models.IntegerField(db_column='CustomerId')),
                ('invoice_date', models.DateTimeField(db_column='InvoiceDate')),
                ('billing_address', models.TextField(blank=True, db_column='BillingAddress', null=True)),
                ('billing_city', models.TextField(blank=True, db_column='BillingCity', null=True)),
                ('billing_state', models.TextField(blank=True, db_column='BillingState', null=True)),
                ('billing_country', models.TextField(blank=True, db_column='BillingCountry', null=True)),
                ('billing_postalcode', models.TextField(blank=True, db_column='BillingPostalCode', null=True)),
                ('total', models.TextField(db_column='Total')),
            ],
            options={
                'db_table': 'invoices',
                'managed': False,
            },
        ),
    ]
