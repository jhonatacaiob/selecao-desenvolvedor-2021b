from chinook import serializers
from chinook.models import Invoices
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from chinook.serializers import AlbumSerializer, GenreSerializer, PlaylistSerializer, TrackSimplifiedSerializer, CustomerSerializer, InvoiceYearSerializer
from chinook.models import Album, Genre, Playlist, Track, Customer
from core.utils import sql_fetch_all
from django.db.models import Sum


class AlbumListAPIView(ListAPIView):
    queryset = Album.objects.select_related('artist').all()
    serializer_class = AlbumSerializer


class GenreListAPIView(ListAPIView):
    queryset = Genre.objects.prefetch_related('tracks').all()
    #queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class PlaylistListAPIView(ListAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer


class TrackListAPIView(ListAPIView):
    queryset = Track.objects.prefetch_related('album').all() 
    serializer_class = TrackSimplifiedSerializer


class ReportDataAPIView(GenericAPIView):
    def get(self, request, *args, **kwargs):
        
        dado = sql_fetch_all(
            """
                SELECT (c.FirstName || ' ' || c.LastName) AS FullName, 
                c.*, e.Title AS TitleEmployee, e.ReportsTo,
                (e.FirstName || ' ' || e.LastName) AS EmployeeFullName
                FROM customers as c
                LEFT JOIN employees as e ON e.[EmployeeId] = c.[SupportRepId]
                ORDER BY CustomerId
            """
        )

        return Response(dado)


class CustomerList(APIView):

    def get(self):
        customers = Customer.objects.all()
        serializer = CustomerSerializer(customers, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InvoiceYear(APIView):
    def post(self, request):
       
        serializer = InvoiceYearSerializer(data=request.data)
        if serializer.is_valid():
            customer_id = request.data['customer_id']
            year = str(request.data['year'])

            invoices = Invoices.objects.filter(customer_id = customer_id, invoice_date__startswith = year)
            
            if(len(invoices) > 0):
                total = invoices.aggregate(Sum('total'))
                total['total__sum'] = '%.2f' %(total['total__sum'])
                total['total_gasto_ano'] = total.pop('total__sum')
            
            else:
                total = {'total_gasto_ano': '%.2f' %(0)}

            return Response(total, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
