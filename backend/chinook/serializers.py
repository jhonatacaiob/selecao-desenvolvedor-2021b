from rest_framework import serializers
from chinook.models import Album, Artist, Genre, Playlist, Track, Customer, Invoices

class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = '__all__'
        depth = 1


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):
    tracks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Genre
        fields = '__all__'


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = '__all__'


class TrackSimplifiedSerializer(serializers.ModelSerializer):
    album_name = serializers.CharField(source='album.title', read_only=True)
    duration = serializers.SerializerMethodField()

    def get_duration(self, obj):
        total_seconds = obj.milliseconds/1000

        minutes = total_seconds // 60
        seconds = total_seconds - minutes*60

        return f'{minutes}min {seconds}s'

    class Meta:
        model = Track
        fields = ('id', 'name', 'composer', 'album_name', 'duration')


class PlaylistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Playlist
        fields = ('name', )


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = '__all__'


class InvoiceYearSerializer(serializers.ModelSerializer):

    def validate_year(self, value):
        
        if(isinstance(value, int) and (value < 2100) and (value > 1970)):
            return value
        
        raise serializers.ValidationError("Insira um ano válido")

    def validate_customer_id(self, value):
        if(Customer.objects.filter(pk=value).exists() and isinstance(value, int)):
            return value
        
        raise serializers.ValidationError("Insira um ID válido")


    class Meta:
        model = Invoices
        fields = ('customer_id', )