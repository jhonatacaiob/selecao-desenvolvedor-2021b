# Prova do backend

1 - A rota `/albums/` está retornando uma listagem dos albuns com os artistas do album, porém em alguns de nossos clientes
    que utilizam bancos armazenados em servidores próprios ela está muito lenta. Ao acompanhar as consultas SQL com um
    profiler foi detectado que está sendo realizado muitas consultas no banco e isso está causando a lentidão.
    Corrigir essa situação, explique o que causava esta situação e como foi resolvido.

    Resposta: Ao realizar a requisiçao na rota '/albums/', o django retornava a lista de albuns, e depois realisava uma consulta para cada 
    registro da chave estrangeira da tabela artists. Solucionado ao adcionar o metodo select_related, que resumidamente, adcionou um join a 
    consulta inicial, e reduzindo assim o custo de operação 

2 - A rota `/genres/` também está com um problema de otimização de consultas SQL, sendo realizada várias consultas
    seguidas no banco causando lentidão. Resolva esta situação, explique o que causava o problema e o como a correção
    realizada resolve esse problema.

    Resposta: Problema semelhante a questão 1, porem estamos fazendo uma requisição em uma tabela que não possui chave estrangeira.
    Para isso usaremos o prefetch_related, ele fará duas consultas no banco, uma para "genres", e outra para "tracks". Logo depois, 
    unirá as duas consultas no python, nesse processo, ele vai pegar todos os "id's" de "genres", e comparar com a chave estrangeira de "tracks",
    e no fim, retornará a lista de todas as tracks separadas por genero. 

3 - A rota `/tracks/` está com problemas de performance. Nossos clientes não estão conseguindo utilizar ela para consultar
    a lista de faixas disponíveis. Mesmo em nosso banco de testes está demorando muito para carregar.
    Analisar, identificar o problema, corrigir e explicar a causa do problema e o que foi feito para solucionar.

    Resposta: O método .only está fazendo uma consulta por coluna, e isso ta congestionando o banco. a solução foi retirá-lo. 
    Ao retirar, o django faz algo semelhante a questão 1 e 2. Uma requisição para cada chave estrangeira na tabela "tracks"
    a solução é usar ou um select_related ou prefetch_related, eu escolhi prefetch_related por uma questão de performance.

4 - Crie uma rota `/customer_simplified/` para realizar o cadastro simplificado de clientes.
    Esta rota deverá aceitar somente os campos obrigatórios do cadastro, e deverá fazer a validação dos campos, e do
    e-mail conforme o padrão de funcionamento do Django-Rest-Framework.

    Resposta: Rota criada e direcionada para uma CBV, onde se encontra um metodo post declarado, e nele os dados são serializados e salvos no banco. Quanto a validação, ao mudar o tipo de campo de "email" na model "customer" o django já faz todo o trabalho de validação de email. 

5 - A rota `/playlists/` não está funcionando. Conserte seu funcionamento e explique o motivo que causou o erro no código.
    
    Resposta: o campo "Field" no "PlaylistSerializer" aceita ou a string "__all__" ou uma lista ou uma tupla, o termo ("name") estava sendo lido como uma string, a solução é adcionar uma virgula após o name, e "forçar" o python a ler como tupla

6 - Crie uma API que receberá um objeto JSON em uma solicitação POST. Este objeto JSON conterá `customer_id` e `year`.
    Esta rota deverá validar os dados recebidos usando um serializer. E deverá fazer as seguintes validações:
    - `customer_id` é um número inteiro, e existe um cliente com este ID no banco.
    - `year` é um número inteiro, e está entre 1970 e 2100.
    Se os dados recebidos passarem na validação, o retorno dessa API deverá ser o total gasto por este cliente no
    ano determinado.
    
    Reposta: API criada com o nome de `invoice_year`

7 - Na rota `/report_data/` está retornando os dados de clientes através de consulta SQL pura. Porém, ao realizar
    uma consulta nessa rota, foi observado que o cliente de ID=1 (Luís Gonçalves) está retornando com FirstName=Jane e
    LastName=Peacock, enquanto isto, o novo campo criado FullName que é a concatenação dos dois, está retornando
    "Luís Gonçalves" corretamente. Ao realizar a consulta no cliente SQL, a consulta traz os dados corretamente,
    porém ao colocar a mesma consulta na API, os dados da API retornam errados. Identifique a causa do problema,
    explique o motivo de ter ocorrido, e corrija-o.

    Resposta: A tabela employee e a tabela customers tem algumas colunas com exatamente o mesmo nome, ao realizarmos o select do join o server do banco se confunde com qual das tabelas ele precisa pegar os valores. No caso do cliente SQL, é escolhido os dados de customers, e no caso do Django é escolhido os dados do employee. A solução é dizer explicitamente qual coluna eu quero que o sql retorne. No caso, eu disse para o banco pegar todos os campos de customers e somente os dados relevantes de employee
